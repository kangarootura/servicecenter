﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ServiceCenter.Models;

namespace ServiceCenter.Controllers
{
    public class HomeController : Controller
    {
        ContextRepository context;
        ApplicationDbContext db;

        private void KeepData()
        {
            ViewBag.userFirstName = (string)TempData.Peek("FirstName");
            ViewBag.userLastName = (string)TempData.Peek("LastName");
            ViewBag.userPatronymicName = (string)TempData.Peek("PatronymicName");
            ViewBag.userID = (int)TempData.Peek("UserID");
            ViewBag.userAccessRights = (string)TempData.Peek("UserRights");
            ViewBag.user = (string)TempData.Peek("UserEmail");
            ViewBag.userDepartment = (int)TempData.Peek("DepartmentID");
        }

        public HomeController(ApplicationDbContext ctx)
        {
            context = new ContextRepository(ctx);
            db = ctx;
        }

        public ViewResult Welcome(User user)
        {
            KeepData();
            return View(context.user);
        }

        public IActionResult Index()
        {
            ViewBag.user = "";
            return View();
        }

        public ViewResult Department()
        {
            KeepData();
           
            return View(context.DisplayDepartmentsWithUsers());
        }
        public ActionResult InfoUsers(string searching)
        {
            KeepData();
            return View (db.Users.Where(x=>x.FirstName.Contains(searching) || searching == null).ToList());
        }

        public ViewResult MyRequests()
        {
            KeepData();

            int key = (int)TempData.Peek("UserID");

            TempData.Keep("UserId");
            TempData.Keep("UserRights");
            var res = context.DisplayMyRequests(key).ToList();
            return View(res);
        }

        public ActionResult AddNewDepartment()
        {
            KeepData();
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        public IActionResult AddedDepartment(Department department)
        {
            KeepData();
            try
            {
                context.AddDepartment(department);
                return View();
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        public ActionResult AddNewUser()
        {
            KeepData();

            return View();
        }

        public IActionResult AddedUser(User user)
        {
            KeepData();

            context.AddNewUser(user);
            return View();

        }

        [HttpGet]
        public ViewResult Login()
        {
            ViewBag.user = "";
            return View();
        }
        [HttpPost]
        public IActionResult Login(string login, string password)
        {
            ViewBag.user = "";

            string authData = "";

            List<User> list = db.Users.ToList();
            User LoggedUser = new User();
            foreach (User user in list)
            {
                user.Email = user.Email.Replace(" ", "");
                user.Password = user.Password.Replace(" ", "");
                if (String.Compare(login, user.Email) == 0 && String.Compare(password, user.Password) == 0)
                {
                    LoggedUser = user;
                    authData = $"Login: {login}   Password: {password}";
                }
                else
                    authData = "Login or password is incorrect";
            }
            if (LoggedUser.Email == login && LoggedUser.Password == password)
            {
                LoggedUser.UserAccessRights = LoggedUser.UserAccessRights.Replace(" ", "");

                if (LoggedUser.UserID != 0)
                {
                    TempData["UserId"] = LoggedUser.UserID;
                }
                if (LoggedUser.UserAccessRights != null)
                {
                    TempData["UserRights"] = LoggedUser.UserAccessRights.ToString();
                }
                if (LoggedUser.Email != null)
                {
                    TempData["UserEmail"] = LoggedUser.Email;
                }
                if (LoggedUser.DepartmentID != 0)
                {
                    TempData["DepartmentID"] = LoggedUser.DepartmentID;
                }
                if (LoggedUser.FirstName != null)
                {
                    TempData["FirstName"] = LoggedUser.FirstName;
                }
                if (LoggedUser.LastName != null)
                {
                    TempData["LastName"] = LoggedUser.LastName;
                }
                if (LoggedUser.FirstName != null)
                {
                    TempData["PatronymicName"] = LoggedUser.PatronymicName;
                }

                context.user.FirstName = (string)TempData.Peek("FirstName");
                context.user.LastName = (string)TempData.Peek("LastName");
                context.user.PatronymicName = (string)TempData.Peek("PatronymicName");
                context.user.UserID = (int)TempData.Peek("UserID");
                context.user.UserAccessRights = (string)TempData.Peek("UserRights");
                context.user.Email = (string)TempData.Peek("UserEmail");
                context.user.DepartmentID = (int)TempData.Peek("DepartmentID");

                KeepData();

                return View("Welcome", context.user);
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult Admin()
        {
            return View("Admin");
        }

        [HttpPost]
        public IActionResult Admin(string email, string password, string rights,
            string firstname, string lastname, string patronymicname)
        {
            User user = new User();
            user.Email = email;
            user.Password = password;
            user.UserAccessRights = rights;
            user.FirstName = firstname;
            user.LastName = lastname;
            user.PatronymicName = patronymicname;
            user.DepartmentID = 1;
            user.UserID = db.Users.Count();

            db.Users.Add(user);
            db.SaveChanges();

            return Content("OK");
        }

        public ActionResult EditDepartment(int ID)
        {
            KeepData();
            var items = db.Users.ToList();
            if (items != null)
            {
                ViewBag.data = items;
            }
            try
            {
                return View(context.GetDepartment(ID));
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        public IActionResult EditedDepartment(int ID, Department department)
        {
            KeepData();
            try
            {
                context.UpdateDepartment(ID, department);
                return View();
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        public ActionResult EditUser(int ID)
        {
            KeepData();
            try
            {
                return View(context.GetUser(ID));
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        public IActionResult EditedUser(int ID, User user)
        {
            KeepData();
            try
            {
                context.UpdateUser(ID, user);
                return View();
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }
        public ActionResult EditProfile()
        {
            KeepData();
            try
            {
                return View(new User() { UserID =  ViewBag.userID});
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        public IActionResult EditedProfile (int ID, User user)
        {
            KeepData();
            try
            {
                context.UpdateProfile(ID, user);
                return View();
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }

            //TempData["UserEmail"] = user.Email;
            //context.user.Email = (string)TempData.Peek("UserEmail");
            KeepData();
           
        }

        public ActionResult DeletedDepartment(int ID)
        {
            KeepData();
            context.DeleteDepartment(ID);
            return View();
        }

        public ActionResult DeletedUser(int ID)
        {
            KeepData();
            context.DeletedUser(ID);
            return View();
        }

        public ActionResult RequestForm()
        {
            KeepData();
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }

        }

        public IActionResult AddedRequest(Request request)
        {
            KeepData();
            try
            {
                request.UserCreatorID = (int)TempData.Peek("UserID");

                TempData.Keep("UserId");
                TempData.Keep("UserRights");
                context.AddRequest(request);
                return View();


            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        public IActionResult ShowDepartmentRequests()
        {
            KeepData();
            int key = (int)TempData.Peek("UserID");

            TempData.Keep("UserId");
            TempData.Keep("UserRights");
            var res = context.DisplayRequestsByDepartment(key).ToList();
            return View(res);
        }

        public IActionResult ShowDepartmentsRequests()
        {
            KeepData(); ;
            return View(context.DisplayRequestsByDepartment().ToList());
        }

        public ActionResult CloseRequest(int ID)
        {
            KeepData();
            try
            {
                return View(new Request { RequestID = ID});
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }

        public IActionResult ClosedRequest(int ID, Request request)
        {
            KeepData();

            context.CloseRequest(ID, request);
            return View();
        }

        public IActionResult AcceptedRequest(int ID)
        {
            KeepData();
            context.AcceptRequest(ID);
            return View();
        }

        public IActionResult CompletedRequest(int ID)
        {
            KeepData();
            context.CompleteRequest(ID);
            return View();
        }


        
    }
}
