﻿//var but = document.getElementsByClassName("btn btn-info");
//for (var i = 0; i < but.length; i++) {
//   but[i].setAttribute("id",'shape'+i)
//}
//var tb = document.getElementsByClassName("table table-sm table-striped table-bordered")
//for (var i = 0; i < tb.length; i++) {
//    tb[i].setAttribute("id", 'shape' + i+'-reveal')
//}
$(".btn.btn-info").click(function () {
    $(this).nextAll("table").slideToggle("fast");
    if ($.trim($(this).text()) === 'Show') {
        $(this).text('Hide');
    } else {
        $(this).text('Show');
    }
});
    
var numInputs = document.querySelectorAll('input[type="number"]');

// Loop through the collection and call addListener on each element
Array.prototype.forEach.call(numInputs, addListener); 

function addListener(elm, index) {
    elm.setAttribute('min', 1);  // set the min attribute on each field

    elm.addEventListener('keypress', function (e) {  // add listener to each field 
        var key = !isNaN(e.charCode) ? e.charCode : e.keyCode;
        str = String.fromCharCode(key);
        if (str.localeCompare('-') === 0) {
            event.preventDefault();
        }

    });

}