﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServiceCenter.Migrations
{
    public partial class initialUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 1,
                column: "MainManager",
                value: 1);

            migrationBuilder.UpdateData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 2,
                column: "MainManager",
                value: 3);

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserID", "DepartmentID", "Email", "FirstName", "LastName", "Password", "PatronymicName", "UserAccessRights" },
                values: new object[,]
                {
                    { 1, 1, "JaneShane@awesome.com", "Jane", "Kruh", "1111", "Ivanovna", "manager" },
                    { 2, 1, "MukPuk@awesome.com", "Muk", "Puk", "2222", "Tuk", "employee" },
                    { 3, 2, "LupaPupa@awesome.com", "Lupa", "Pupa", "3333", "Kuka", "manager" },
                    { 4, 2, "LupPup@awesome.com", "Lup", "Pup", "4444", "Kuk", "employee" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 4);

            migrationBuilder.UpdateData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 1,
                column: "MainManager",
                value: 0);

            migrationBuilder.UpdateData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 2,
                column: "MainManager",
                value: 0);
        }
    }
}
