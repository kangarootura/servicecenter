﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ServiceCenter.Models;

namespace ServiceCenter.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20190414190050_initial-manager")]
    partial class initialmanager
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.8-servicing-32085")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ServiceCenter.Models.Department", b =>
                {
                    b.Property<int>("DepartmentID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description");

                    b.Property<int>("MainManager");

                    b.Property<string>("Name");

                    b.HasKey("DepartmentID");

                    b.ToTable("Departments");

                    b.HasData(
                        new { DepartmentID = 1, Description = "first department", MainManager = 1, Name = "One" },
                        new { DepartmentID = 2, Description = "second department", MainManager = 3, Name = "Two" }
                    );
                });

            modelBuilder.Entity("ServiceCenter.Models.Request", b =>
                {
                    b.Property<int>("RequestID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description");

                    b.Property<string>("RequestCategory");

                    b.Property<string>("RequestStatus");

                    b.Property<int>("UserCreatorID");

                    b.HasKey("RequestID");

                    b.HasIndex("UserCreatorID");

                    b.ToTable("Requests");
                });

            modelBuilder.Entity("ServiceCenter.Models.User", b =>
                {
                    b.Property<int>("UserID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("DepartmentID");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Password");

                    b.Property<string>("PatronymicName");

                    b.Property<string>("UserAccessRights");

                    b.HasKey("UserID");

                    b.HasIndex("DepartmentID");

                    b.ToTable("Users");

                    b.HasData(
                        new { UserID = 1, DepartmentID = 1, Email = "JaneShane@awesome.com", FirstName = "Jane", LastName = "Kruh", Password = "1111", PatronymicName = "Ivanovna", UserAccessRights = "manager" },
                        new { UserID = 2, DepartmentID = 1, Email = "MukPuk@awesome.com", FirstName = "Muk", LastName = "Puk", Password = "2222", PatronymicName = "Tuk", UserAccessRights = "employee" },
                        new { UserID = 5, DepartmentID = 1, Email = "fukuuk@awesome.com", FirstName = "fuk", LastName = "uuk", Password = "2222", PatronymicName = "suk", UserAccessRights = "manager" },
                        new { UserID = 3, DepartmentID = 2, Email = "LupaPupa@awesome.com", FirstName = "Lupa", LastName = "Pupa", Password = "3333", PatronymicName = "Kuka", UserAccessRights = "manager" },
                        new { UserID = 4, DepartmentID = 2, Email = "LupPup@awesome.com", FirstName = "Lup", LastName = "Pup", Password = "4444", PatronymicName = "Kuk", UserAccessRights = "employee" }
                    );
                });

            modelBuilder.Entity("ServiceCenter.Models.Request", b =>
                {
                    b.HasOne("ServiceCenter.Models.User", "User")
                        .WithMany("Requests")
                        .HasForeignKey("UserCreatorID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ServiceCenter.Models.User", b =>
                {
                    b.HasOne("ServiceCenter.Models.Department", "Department")
                        .WithMany("Users")
                        .HasForeignKey("DepartmentID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
