﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServiceCenter.Migrations
{
    public partial class initialmanager : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserID", "DepartmentID", "Email", "FirstName", "LastName", "Password", "PatronymicName", "UserAccessRights" },
                values: new object[] { 5, 1, "fukuuk@awesome.com", "fuk", "uuk", "2222", "suk", "manager" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 5);
        }
    }
}
