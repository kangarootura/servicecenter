﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServiceCenter.Migrations
{
    public partial class initialagain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "DepartmentID", "Description", "MainManager", "Name" },
                values: new object[] { 1, "first department", 0, "One" });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "DepartmentID", "Description", "MainManager", "Name" },
                values: new object[] { 2, "second department", 0, "Two" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 2);
        }
    }
}
