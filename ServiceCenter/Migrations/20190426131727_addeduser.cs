﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServiceCenter.Migrations
{
    public partial class addeduser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserID", "DepartmentID", "Email", "FirstName", "LastName", "Password", "PatronymicName", "UserAccessRights" },
                values: new object[] { 6, 1, "Admin@awesome.com", "Admin", "Adminn", "6666", "Adminnn", "admin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 6);
        }
    }
}
