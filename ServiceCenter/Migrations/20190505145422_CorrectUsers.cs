﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServiceCenter.Migrations
{
    public partial class CorrectUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 6);

            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "Requests",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 1,
                columns: new[] { "Description", "MainManager", "Name" },
                values: new object[] { "Department with new employees", 0, "Newcomers employees" });

            migrationBuilder.UpdateData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 2,
                columns: new[] { "Description", "MainManager", "Name" },
                values: new object[] { "first department", 1, "One" });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "DepartmentID", "Description", "MainManager", "Name" },
                values: new object[] { 3, "second department", 4, "Two" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 1,
                columns: new[] { "Email", "FirstName", "LastName", "UserAccessRights" },
                values: new object[] { "j4n3sh4n3@gmail.com", "Yevheniia", "Krukhmalova", "admin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 2,
                columns: new[] { "FirstName", "LastName", "PatronymicName" },
                values: new object[] { "Anton", "Mikhaylenko", "Batkovich" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 3,
                columns: new[] { "Email", "FirstName", "LastName", "PatronymicName", "UserAccessRights" },
                values: new object[] { "fukuuk@awesome.com", "Andrew", "Karachevtsev", "Batkovich", "employee" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 4,
                columns: new[] { "DepartmentID", "Email", "FirstName", "LastName", "PatronymicName", "UserAccessRights" },
                values: new object[] { 1, "LupaPupa@awesome.com", "Yurii", "Bidashko", "Batkovich", "manager" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 5,
                columns: new[] { "DepartmentID", "Email", "FirstName", "LastName", "Password", "PatronymicName" },
                values: new object[] { 2, "LupPup@awesome.com", "Vlad", "Kuznetsov", "55555", "Batkovich" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 3);

            migrationBuilder.DropColumn(
                name: "Comment",
                table: "Requests");

            migrationBuilder.UpdateData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 1,
                columns: new[] { "Description", "MainManager", "Name" },
                values: new object[] { "first department", 1, "One" });

            migrationBuilder.UpdateData(
                table: "Departments",
                keyColumn: "DepartmentID",
                keyValue: 2,
                columns: new[] { "Description", "MainManager", "Name" },
                values: new object[] { "second department", 3, "Two" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 1,
                columns: new[] { "Email", "FirstName", "LastName", "UserAccessRights" },
                values: new object[] { "JaneShane@awesome.com", "Jane", "Kruh", "manager" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 2,
                columns: new[] { "FirstName", "LastName", "PatronymicName" },
                values: new object[] { "Muk", "Puk", "Tuk" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 3,
                columns: new[] { "Email", "FirstName", "LastName", "PatronymicName", "UserAccessRights" },
                values: new object[] { "LupaPupa@awesome.com", "Lupa", "Pupa", "Kuka", "manager" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 4,
                columns: new[] { "DepartmentID", "Email", "FirstName", "LastName", "PatronymicName", "UserAccessRights" },
                values: new object[] { 2, "LupPup@awesome.com", "Lup", "Pup", "Kuk", "employee" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 5,
                columns: new[] { "DepartmentID", "Email", "FirstName", "LastName", "Password", "PatronymicName" },
                values: new object[] { 1, "fukuuk@awesome.com", "fuk", "uuk", "2222", "suk" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserID", "DepartmentID", "Email", "FirstName", "LastName", "Password", "PatronymicName", "UserAccessRights" },
                values: new object[] { 6, 1, "Admin@awesome.com", "Admin", "Adminn", "6666", "Adminnn", "admin" });
        }
    }
}
