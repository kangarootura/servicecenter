﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ServiceCenter.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public ApplicationDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne<Department>(u => u.Department)
                .WithMany(d => d.Users)
                .HasForeignKey(u => u.DepartmentID);

            modelBuilder.Entity<Request>()
                .HasOne<User>(r => r.User)
                .WithMany(u => u.Requests)
                .HasForeignKey(r => r.UserCreatorID);

            modelBuilder.Entity<Department>().HasData(
                new Department()
                {
                    DepartmentID = 00001,
                    Name = "Newcomers employees",
                    MainManager = 0,
                    Description = "Department with new employees"
                },
                new Department()
                {
                    DepartmentID = 00002,
                    Name = "One",
                    MainManager = 4,
                    Description = "first department"
                },
                new Department()
                {
                    DepartmentID = 00003,
                    Name = "Two",
                    MainManager = 5,
                    Description = "second department"
                });

            modelBuilder.Entity<User>().HasData(
                new User()
                {
                    UserID = 1,
                    FirstName = "Yevheniia",
                    LastName = "Krukhmalova",
                    PatronymicName = "Ivanovna",
                    UserAccessRights = UserAccessRights.admin.ToString(),
                    DepartmentID = 00002,
                    Password = "1111",
                    Email = "j4n3sh4n3@gmail.com",
                },
                 new User()
                 {
                     UserID = 2,
                     FirstName = "Anton",
                     LastName = "Mikhaylenko",
                     PatronymicName = "Batkovich",
                     UserAccessRights = UserAccessRights.employee.ToString(),
                     DepartmentID = 00002,
                     Password = "1111",
                     Email = "Mikhaylenko.Anton@awesome.com",
                 },
                 new User()
                 {
                     UserID = 3,
                     FirstName = "Andrew",
                     LastName = "Karachevtsev",
                     PatronymicName = "Batkovich",
                     UserAccessRights = UserAccessRights.employee.ToString(),
                     DepartmentID = 00003,
                     Password = "1111",
                     Email = "Karachevtsev.Andrew@awesome.com",
                 },
                  new User()
                  {
                      UserID = 4,
                      FirstName = "Yurii",
                      LastName = "Bidashko",
                      PatronymicName = "Batkovich",
                      UserAccessRights = UserAccessRights.manager.ToString(),
                      DepartmentID = 00002,
                      Password = "1111",
                      Email = "Bidashko.Yurii@awesome.com",
                  },
                   new User()
                   {
                       UserID = 5,
                       FirstName = "Vlad",
                       LastName = "Kuznetsov",
                       PatronymicName = "Batkovich",
                       UserAccessRights = UserAccessRights.manager.ToString(),
                       DepartmentID = 00003,
                       Password = "1111",
                       Email = "Kuznetsov.Vlad@awesome.com",
                   });

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Request> Requests { get; set; }
    }
    
}
