﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth;
using System.Security.Cryptography.X509Certificates;

namespace ServiceCenter.Models
{
    public class ContextRepository : IContextRepository
    {
        private ApplicationDbContext context;

        public User user = new User();

        public ContextRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public ApplicationDbContext Context => context;

        public void AddDepartment(Department department)
        {
            if (department.Name != null && department.Description != null)
            {
                context.Departments.Add(department);
                context.SaveChanges();
            }
        }

        public void AddNewUser(User user)
        {
            if (user.Email != null && user.Password != null && user.FirstName != null && user.LastName != null && user.PatronymicName != null)
            {
                user.DepartmentID = 1;
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        public void UpdateDepartment(int ID, Department department)
        {
            var result = context.Departments.SingleOrDefault(d => d.DepartmentID == ID);
            if (result != null)
            {
                if (department.Name != null)
                {
                    result.Name = department.Name;
                }
                if (department.Description != null)
                {
                    result.Description = department.Description;
                }
                if (department.MainManager != 0)
                {

                    var newManager = (from u in context.Users
                                     where u.UserID == department.MainManager
                                     select u).SingleOrDefault();

                    if (department.MainManager != 0 && newManager != null && newManager.UserAccessRights.ToLower() == UserAccessRights.manager.ToString())
                    {
                        result.MainManager = department.MainManager;
                    }

                    department.MainManager = department.MainManager;
                }
                context.SaveChanges();
            }
        }

        public void UpdateUser(int ID, User _user)
        {
            var result = context.Users.SingleOrDefault(u => u.UserID == ID);
            if (result != null)
            {
                if (_user.Email != null)
                {
                    result.Email = _user.Email;
                }
                if (_user.Password != null)
                {
                    result.Password = _user.Password;
                }
                if (_user.UserAccessRights != null)
                {
                    result.UserAccessRights = _user.UserAccessRights;
                }
                if (_user.DepartmentID != 0)
                {
                    result.DepartmentID = _user.DepartmentID;
                }
                if (_user.FirstName != null)
                {
                    result.FirstName = _user.FirstName;
                }
                if (_user.LastName != null)
                {
                    result.LastName = _user.LastName;
                }
                if (_user.PatronymicName != null)
                {
                    result.PatronymicName = _user.PatronymicName;
                }
                context.SaveChanges();
            }
        }

        public void UpdateProfile(int ID, User user)
        {
            var result = context.Users.SingleOrDefault(u => u.UserID == ID);
            if (result != null)
            {
                if (user.Email != null)
                {
                    result.Email = user.Email;
                }
                if (user.Password != null)
                {
                    result.Password = user.Password;
                }
                if (user.FirstName != null)
                {
                    result.FirstName = user.FirstName;
                }
                if (user.LastName != null)
                {
                    result.LastName = user.LastName;
                }
                if (user.PatronymicName != null)
                {
                    result.PatronymicName = user.PatronymicName;
                }
                context.SaveChanges();
            }
        }

        public void DeleteDepartment(int ID)
        {
            var result = context.Departments.SingleOrDefault(d => d.DepartmentID == ID);
            if (result != null)
            {
                Context.Departments.Remove(result);
                context.SaveChanges();
            }
        }

        public void AddRequest(Request request)
        {
            if (request.UserCreatorID != 0  && request.Description != null)
            {
                request.RequestStatus = RequestStatus.created.ToString();
                context.Requests.Add(request);
                context.SaveChanges();
            }
        }

        public async void CloseRequest(int ID, Request request)
        {
            var result = context.Requests.SingleOrDefault(d => d.RequestID == ID);
            if (result != null)
            {
                result.RequestStatus = RequestStatus.closed.ToString();
                result.Comment = request.Comment;
                context.SaveChanges();
                var user = (from u in context.Users where u.UserID == result.UserCreatorID select u).SingleOrDefault();

                var department = (from d in context.Departments where d.DepartmentID == user.DepartmentID select d).SingleOrDefault();

                var manager = (from u in context.Users where u.UserID == department.MainManager select u).SingleOrDefault();

                var admins = (from u in context.Users where u.UserAccessRights == "admin" select u).ToList();

                if (user != null)
                {
                    await SendMessage("drunyakk@gmail.com"/*user.Email*/, "Close Request", "User with id: " + user.UserID.ToString() + " Request with id: " + result.RequestID + ", with description: \"" + result.Description + "\" status was changed on: \"" + result.RequestStatus + "\" with comment: " + result.Comment + "\"");
                }
                if (manager != null)
                {
                    await SendMessage("drunyakk@gmail.com"/*manager.Email*/, "Close Request", "User with id: " + manager.UserID.ToString() + " Request with id: " + result.RequestID + ", with description: \"" + result.Description + "\" status was changed on: \"" + result.RequestStatus + "\" with comment: " + result.Comment + "\"");
                    if (admins != null)
                    {
                        foreach (var admin in admins)
                        {
                            await SendMessage("drunyakk@gmail.com"/*admin.Email*/, "Close Request", "User with id: " + admin.UserID.ToString() + " Request with id: " + result.RequestID + ", with description: \"" + result.Description + "\" status was changed on: \"" + result.RequestStatus + "\" with comment: " + result.Comment + "\"");
                        }
                    }
                }
            }
                
        }

        public async void AcceptRequest(int ID)
        {
            var result = context.Requests.SingleOrDefault(d => d.RequestID == ID);
            if (result != null)
            {
                result.RequestStatus = RequestStatus.accepted.ToString();
                context.SaveChanges();

                var user = (from u in context.Users where u.UserID == result.UserCreatorID select u).SingleOrDefault();

                var department = (from d in context.Departments where d.DepartmentID == user.DepartmentID select d).SingleOrDefault();

                var manager = (from u in context.Users where u.UserID == department.MainManager select u).SingleOrDefault();

                var admins = (from u in context.Users where u.UserAccessRights == "admin" select u).ToList();

                if (user != null)
                {
                    await SendMessage("drunyakk@gmail.com"/*user.Email*/, "Accept Request", "User with id: " + user.UserID.ToString() + " Request with id: " + result.RequestID + ", with description: \"" + result.Description + "\" status was changed on: \"" + result.RequestStatus +  "\"");
                }
                if (manager != null)
                {
                    await SendMessage("drunyakk@gmail.com"/*manager.Email*/, "Accept Request", "User with id: " + manager.UserID.ToString() + " Request with id: " + result.RequestID + ", with description: \"" + result.Description + "\" status was changed on: \"" + result.RequestStatus + "\"");
                    if (admins != null)
                    {
                        foreach (var admin in admins)
                        {
                            await SendMessage("drunyakk@gmail.com"/*admin.Email*/, "Accept Request", "User with id: " + admin.UserID.ToString() + " Request with id: " + result.RequestID + ", with description: \"" + result.Description + "\" status was changed on: \"" + result.RequestStatus + "\"");
                        }
                    }
                }
            } 
        }

        public async void CompleteRequest(int ID)
        {
            var result = context.Requests.SingleOrDefault(d => d.RequestID == ID);
            if (result != null)
            {
                result.RequestStatus = RequestStatus.completed.ToString();
                context.SaveChanges();

                var user = (from u in context.Users where u.UserID == result.UserCreatorID select u).SingleOrDefault();

                var department = (from d in context.Departments where d.DepartmentID == user.DepartmentID select d).SingleOrDefault();

                var manager = (from u in context.Users where u.UserID == department.MainManager select u).SingleOrDefault();

                var admins = (from u in context.Users where u.UserAccessRights == "admin" select u).ToList();

                if (user != null)
                {
                    await SendMessage("drunyakk@gmail.com"/*user.Email*/, "Complete Request", "User with id: " + user.UserID.ToString() + " Request with id: " + result.RequestID + ", with description: \"" + result.Description + "\" status was changed on: \"" + result.RequestStatus + "\"");
                }
                if (manager != null)
                {
                    await SendMessage("drunyakk@gmail.com"/*manager.Email*/, "Complete Request", "User with id: " + manager.UserID.ToString() + " Request with id: " + result.RequestID + ", with description: \"" + result.Description + "\" status was changed on: \"" + result.RequestStatus + "\"");
                    if (admins != null)
                    {
                        foreach (var admin in admins)
                        {
                            await SendMessage("drunyakk@gmail.com"/*admin.Email*/, "Complete Request", "User with id: " + admin.UserID.ToString() + " Request with id: " + result.RequestID + ", with description: \"" + result.Description + "\" status was changed on: \"" + result.RequestStatus + "\"");
                        }
                    }
                }
            }
        }

        public IEnumerable<Request> DisplayMyRequests(int usserId)
        {
            var requests = (from r in context.Requests where r.UserCreatorID == usserId select r).ToList().OrderBy(r => r.RequestStatus);
            return requests;
        }

        public IEnumerable<DepartmentWithUserWithRequest> DisplayDepartmentsWithUsers()
        {
            var list = context.Departments.GroupJoin(context.Users,
                                                     d => d.DepartmentID,
                                                     u => u.DepartmentID,
                                                     (dep, users) => new DepartmentWithUserWithRequest()
                                                     {
                                                         department = dep,
                                                         Users = users.ToList()
                                                     });

            return list;
        }

        public IEnumerable<DepartmentWithUserWithRequest> DisplayRequestsByDepartment(int managerId)
        {
           var list = (from d in context.Departments
                       where d.MainManager == managerId
                       select new DepartmentWithUserWithRequest()
                       {
                           department = d,
                           Users = (from u in context.Users
                                    where u.DepartmentID == d.DepartmentID
                                    select u).ToList(),
                           Requests = (from r in context.Requests
                                       join u in context.Users on r.UserCreatorID equals u.UserID
                                       where u.DepartmentID == d.DepartmentID
                                       select r).ToList()

                       }).ToList();

            return list;
        }

        public IEnumerable<DepartmentWithUserWithRequest> DisplayRequestsByDepartment()
        {
            var list = (from d in context.Departments
                        select new DepartmentWithUserWithRequest()
                        {
                            department = d,
                            Users = (from u in context.Users
                                     where u.DepartmentID == d.DepartmentID
                                     select u).ToList(),
                            Requests = (from r in context.Requests
                                        join u in context.Users on r.UserCreatorID equals u.UserID
                                        where u.DepartmentID == d.DepartmentID
                                        select r).ToList()

                        }).ToList();

            return list;
        }

        public Department GetDepartment(int DepartmentID)
        {
            var department = (from d in context.Departments where d.DepartmentID == DepartmentID select d).FirstOrDefault();
            return department;
        }

        public User GetUser(int UserID)
        {
            var user = (from u in context.Users where u.UserID == UserID select u).FirstOrDefault();
            return user;
        }

        public DepartmentWithUserWithRequest GetDepartmentWithUsers(int departmentID)
        {
            DepartmentWithUserWithRequest departmentWithUsers = new DepartmentWithUserWithRequest();

            var department = (from d in context.Departments where d.DepartmentID == departmentID select d).FirstOrDefault();
            var users = (from u in context.Users where u.DepartmentID == departmentID && u.UserAccessRights == UserAccessRights.manager.ToString() select u).ToList();

            departmentWithUsers.department = department;
            departmentWithUsers.Users = users;

            return departmentWithUsers;
        }

        public void DeletedUser(int ID)
        {
            var result = context.Users.SingleOrDefault(u => u.UserID == ID);
            if (result != null)
            {
                Context.Users.Remove(result);
                context.SaveChanges();
            }
        }

        public async Task SendMessage(string to, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Your System", "ServiceCenterUA01@gmail.com"));
            emailMessage.To.Add(new MailboxAddress(to));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Plain)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587, false);

                client.AuthenticationMechanisms.Remove("XOAUTH2");
                await client.AuthenticateAsync("ServiceCenterUA01@gmail.com", "v4t7r8p5s9");
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}
