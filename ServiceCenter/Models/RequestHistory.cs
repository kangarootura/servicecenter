﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.Models
{
    public class RequestHistory
    {
        public int RequestHistoryID { get; set; }
        public int RequestID { get; set; }
        public int ManagerID { get; set; }
        public bool IsApproved { get; set; }
    }
}
