﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.Models
{
    public class User
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public int UserID { get; set; }
      
        public int DepartmentID { get; set; }
        public string UserAccessRights { get; set; }
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Не указано имя")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string LastName { get; set; }

        public string PatronymicName { get; set; }

        public Department Department { get; set; }
        public ICollection<Request> Requests { get; set; }
    }
}
