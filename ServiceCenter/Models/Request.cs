﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.Models
{
    public class Request
    {
        public int RequestID { get; set; }
        public int UserCreatorID { get; set; }
        public string RequestStatus { get; set; }
        public string RequestCategory { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }

        public User User { get; set; }
    }
}
