﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.Models
{
    public class RegistrantsViewModel
    {
        public RegistrantsViewModel()
        {
            RaceEvents = new List<SelectListItem>();
        }

        public IEnumerable<ServiceCenter.Models.Department> Registrants { get; set; }

        public List<SelectListItem> RaceEvents { get; set; }

        public Int32 SelectedItem { get; set; }

    }
}
