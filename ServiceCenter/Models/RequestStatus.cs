﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.Models
{
    public enum RequestStatus
    {
        created, accepted, completed, closed
    }
}
