﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCenter.Models
{
    public class DepartmentWithUserWithRequest
    {
        public Department department { get; set; }
        public List<User> Users { get; set; }
        public List<Request> Requests { get; set; }
    }
}
